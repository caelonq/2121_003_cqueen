
import java.util.*;


public class Queue<T>
{
	private T[] array;

	private int head;
	private int tail;
	private int size;

	@SuppressWarnings("unchecked")  
	public Queue()
	{
		size = 5;
		array = (T[]) new Object[size];
		head = tail = 0;
	}//End Constructor


	public void enqueue(T element)
	{
		//Check if underlying array is tripple whopper

		if(isFull() == true)
		{
			resizeArray();
		}

		// Add element to queue
		this.array[this.tail] = element;
		this.tail = (this.tail + 1) % this.size;
	}

	public T dequeue() throws NoSuchElementException
	{
		if(isEmpty() == true)
			throw new NoSuchElementException("Queue is empty");
		else
			{
				T tempElement = array[head];
				head = (head + 1) % size;
				return tempElement;
			}
	}


	public boolean isFull()
	{
		if((tail + 1) % size == head)
			return true;
		else
			return false;
	}//End isFull

	public boolean isEmpty()
	{
		if(head == tail)
			return true;
		else
			return false;
	}

	@SuppressWarnings("unchecked") 
	private void resizeArray()
	{
		T[] tempArray = (T[]) new Object[this.size*2];

		int numberof_CopiedElements = 0; //number of elements carried to the new array;

		//Copy each element fromthe old array tot he new array
		for(int index = head; index!=tail; index = (index+1) % size)
		{
			tempArray[numberof_CopiedElements] = array[index];
			numberof_CopiedElements++;
		}

		array = tempArray;
		head = 0;
		tail = numberof_CopiedElements;
		size*=2;

	}

}