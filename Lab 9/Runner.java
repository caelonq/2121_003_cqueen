public class Runner 
{
	public static void main(String[] args)
	{
		TheOneRing ring = TheOneRing.getInstance();
		ring.castIntoTheFire();

		TheOneRing knockoff = TheOneRing.getInstance();
		System.out.println("ring && knockoff: "+ring.equals(knockoff)+"\n");

		Geist casperheisen = new Geist("Casperheisen");
		casperheisen.herumgeistern();
		casperheisen.erschrecken();

	}
}