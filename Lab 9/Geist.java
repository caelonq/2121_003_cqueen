public class Geist
{
	Ghost casper = null;


	public Geist(String name)
	{
		casper = new Ghost(name);
	}


	/**
	 * Allows this to half-heartedly creep out the user. 
	 */ 
	public void herumgeistern() {
		casper.haunt(); 
	} // end method haunt


	/**
	 * Allows this to startle the user. 
	 */ 
	public void erschrecken() {
		casper.scare();
	} // end method scare
	
}