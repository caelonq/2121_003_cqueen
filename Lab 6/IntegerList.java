import java.util.Arrays;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Collections;
import java.util.PriorityQueue;


public class IntegerList
{
	
	private static Integer[] array = {7, 2, 5, 9, 4, 10, 21, 31, 6, 19, 2, 32, 21};
	private static LinkedList<Integer> list = new LinkedList<Integer>(Arrays.asList(array));
	private static Iterator<Integer> iterator = list.iterator();
	private static PriorityQueue<Integer> queue;	

	public static void main(String[] args) {
		
		System.out.printf("%5s%n", "Current integers in list before list manipulations");
		while(iterator.hasNext())
		{
			Integer integer = iterator.next();
			System.out.printf("%3d", integer);		
		}
			System.out.println("\n\n");

		printLinkedListWithTForLoop();
		printLinkedListWithIterator();
		printPriorityQueue();
		//printLinkedListWithEForLoop();

}

	public static void printLinkedListWithTForLoop()
	{

		for(int i = 0; i < list.size(); ++i)
		{
			if(!(list.get(i) % 2 == 0))
				System.out.printf("Odd Integer: %d%n", list.get(i));
			else
			{
				System.out.printf("Removed Even Integer: %d%n", list.get(i));
				list.remove(list.get(i));
				--i;
			}
		}

		System.out.printf("%8s%n", "Current integers in list after traditional for loop");
		for(Integer numbers: list)
		{
			System.out.printf("%7d", numbers);

		}
		System.out.println("");
	}

	public static void printLinkedListWithIterator()
	{

		list = new LinkedList<Integer>(Arrays.asList(array));

		Iterator<Integer> iterator1 = list.iterator();
		
		//Sentinel Controlled Repetition. (Mode Safe)
		while(iterator1.hasNext())
		{
			Integer integer = iterator1.next();

			if(!(integer % 2 == 0))
				System.out.printf("Odd Integer: %d%n", integer);
			else
			{
				System.out.printf("Removed Even Integer: %d%n", integer);
				iterator1.remove();
			}		
		}
		System.out.printf("%8s%n", "Current integers in list after Iterator");
		for(Integer numbers: list)
		{
			System.out.printf("%7d", numbers);

		}
		System.out.println("");

	}

	public static void printLinkedListWithEForLoop()
	{
		list = new LinkedList<Integer>(Arrays.asList(array));

		for(Integer number: list)
		{
			if(!(number%2 == 0))
				System.out.printf("Odd Integer: %d%n", number);
			else
			{
				System.out.printf("Removed Even Integer: %d%n", number);
				list.remove(number);
			}		
		}

		System.out.printf("%11s%n", "Current integers in list after enhanced for loop");
		for(Integer numbers: list)
		{
			System.out.printf("%15d%n", numbers);
		}
	}

	public static void printPriorityQueue()
	{	
		queue = new PriorityQueue<Integer>(Arrays.asList(array));	
		System.out.printf("%11s%n", "Current integers while polling");
		while(queue.size() > 0)
		{
			System.out.println(queue.peek());
			queue.poll();
		}
	}

}