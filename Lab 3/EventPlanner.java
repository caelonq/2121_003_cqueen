import java.util.ArrayList; 
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.EOFException;

/**
 * Stores a set of <code>Events</code> in an 
 * <code>ArrayList</code> for processing and 
 * printing. Allows the <code>Events</code> 
 * to be serialized and stored to a file for
 * saving them between runs of the program.  
 *
 * @author	Ca3lon Queen
 * @version	Feb/13/2017
 * @see	Event
 * @see Runner
 */ 
public class EventPlanner {
	ArrayList<Event> events; 						// List of Events to process.
	ObjectOutputStream serializationOutput = null; 	// Streams for serialization. 
	ObjectInputStream deserializationInput = null; 
	
	
	/**
	 * Constructor initializes the ArrayList of Events and 
	 * leaves it empty.
	 */ 
	public EventPlanner() {
		this.events = new ArrayList<Event>();
	} // end constructor

	
	/**
	 * Adds two precreated Event instances to the ArrayList.
	 */ 
	public void addEvents() {
        this.events.add(new Event( "Redmann's Party", "2014-03-03 22:00", "Redmann's House", "Come pass a good time" ));
        this.events.add(new Event( "CSCI 2120 Test", "2014-03-24 16:30", "MATH 226", "2nd Test" ));		
	} // end method addEvents
	
	
	/**
	 * Prints the EventPlanner instance's contents.
	 */ 
	public void printEvents() {
		// Planner is empty, inform the user. 
		if (this.events.size() < 1) {
			System.out.println("Planner is empty."); 
		} 
		
		// Otherwise print the Events. 
		else {
			for (Event event : this.events) {
				System.out.println(event.toString());
				System.out.println(); 
			}
		}
	} // end method printEvents
	
	
	/**
	 * Clears all the Events from this. 
	 */ 
	public void clearSchedule() {
		this.events.clear(); 
	} // end method clearSchedule
	
	
	/**
	 * Opens an ObjectOutputStream to serialize the contents
	 * of the ArrayList of Events.
	 */ 
	public void openOutputStream() {

		try
		{
			serializationOutput = new ObjectOutputStream(new FileOutputStream("myEvents.ser"));
		}
		catch(IOException e)
		{
			System.out.println("Error Opening File For Serialization");
			System.exit(1); //Terminate and signal error to operating system
		}
	} // end method openOutputStream 
	
	
	/**
	 * Serializes the Events in the ArrayList and writes 
	 * them to the ObjectOutputStream.
	 */ 
	public void writeObjects() {
	
		try
		{
			for (Event event: events)
			{
				serializationOutput.writeObject(event);
			}
		}
		catch(IOException e)
		{
			System.out.println("Error Writing To Serialization FileOutputStream");
		}
		catch(NullPointerException e)
		{
			System.out.println("OutputStream Not Previously Opened.");
			System.exit(1); // Terminate and signal an error to the OS
		}
	} // end method writeObjects 
	
	
	/**
	 * Closes the ObjectOutputStream.
	 */ 
	public void closeOutputStream() {
		try
		{
			serializationOutput.close();
		}
		catch(IOException e)
		{
			System.out.println("Error Closing Serialization File");
			System.exit(1);
		}
		catch(NullPointerException e)
		{
			System.out.println("OutputStream Not Previously Opened.");
			System.exit(1); // Terminate and signal an error to the OS
		}
	} // end method closeOutputStream
	
	
	/**
	 * Opens an ObjectInputStream to serialize the contents
	 * of the ArrayList of Events.
	 */ 
	public void openInputStream() {
		try
		{
			deserializationInput = new ObjectInputStream(new FileInputStream("myEvents.ser"));
		}
		catch(IOException e)
		{
			System.out.println("Error Opening deserialization File ");
			System.exit(1);
		}
		
		
	} // end method openInputStream
	
	
	/**
	 * Serializes the Events in the ArrayList and reads 
	 * them from the ObjectInputStream.
	 */ 
	public void readObjects() {
		
		try
		{	
			while(true)
			{
				events.add((Event) deserializationInput.readObject());
			}
		}
		catch(NullPointerException e)
		{
			System.out.println("InputStream Not Previously Opened.");
			System.exit(1); // Terminate and signal an error to the OS
		}
		catch(EOFException e)
		{
			System.out.println("Read Through End Of Serialization Events.\n");
		}
		catch(ClassNotFoundException e)
		{
			System.out.println("Object Type Invalid ");
			System.exit(1);
		}
		catch(IOException e)
		{
			System.out.println("Error Reading From Serialization FileInputStream");
		}
	} // end method readObjects
	
	
	/**
	 * Closes the ObjectInputStream.
	 */ 
	public void closeInputStream() {
		try
		{	
			
			deserializationInput.close();
		}
		catch(IOException e)
		{
			System.out.println("Error Closing Serialization File");
		}
		catch(NullPointerException e)
		{
			System.out.println("OutputStream Not Previously Opened.");
			System.exit(1); // Terminate and signal an error to the OS
		}
	} // end method closeInputStream
} // end class Calendar
