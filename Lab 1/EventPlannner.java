import java.util.ArrayList; 

/**
 * Stores a set of <code>Events</code> in an 
 * <code>ArrayList</code> for processing and 
 * printing. Allows the <code>Events</code> 
 * to be serialized and stored to a file for
 * saving them between runs of the program.  
 *
 * @author	YOUR_NAME_HERE
 * @version	DATE_LAST_CHANGED_HERE
 * @see	Event
 * @see Runner
 */ 
public class EventPlanner {
	ArrayList<Event> events; 						// List of Events to process.
	ObjectOutputStream serializationOutput = null; 	// Streams for serialization. 
	ObjectInputStream deserializationInput = null; 
	
	
	/**
	 * Constructor initializes the ArrayList of Events and 
	 * leaves it empty.
	 */ 
	public EventPlanner() {
		this.events = new ArrayList<Event>();
	} // end constructor

	
	/**
	 * Adds two precreated Event instances to the ArrayList.
	 */ 
	public void addEvents() {
        this.events.add(new Event( "Redmann's Party", "2014-03-03 22:00", "Redmann's House", "Come pass a good time" ));
        this.events.add(new Event( "CSCI 2120 Test", "2014-03-24 16:30", "MATH 226", "2nd Test" ));		
	} // end method addEvents
	
	
	/**
	 * Prints the EventPlanner instance's contents.
	 */ 
	public void printEvents() {
		// Planner is empty, inform the user. 
		if (this.events.size() < 1) {
			System.out.println("Planner is empty."); 
		} 
		
		// Otherwise print the Events. 
		else {
			for (Event event : this.events) {
				System.out.println(event.toString());
				System.out.println(); 
			}
		}
	} // end method printEvents
	
	
	/**
	 * Clears all the Events from this. 
	 */ 
	public void clearSchedule() {
		this.events.clear(); 
	} // end method clearSchedule
	
	
	/**
	 * Opens an ObjectOutputStream to serialize the contents
	 * of the ArrayList of Events.
	 */ 
	public void openOutputStream() {
		/* Stubbed */ 
	} // end method openOutputStream 
	
	
	/**
	 * Serializes the Events in the ArrayList and writes 
	 * them to the ObjectOutputStream.
	 */ 
	public void writeObjects() {
		/* Stubbed */ 
	} // end method writeObjects 
	
	
	/**
	 * Closes the ObjectOutputStream.
	 */ 
	public void closeOutputStream() {
		/* Stubbed */
	} // end method closeOutputStream
	
	
	/**
	 * 
	 */ 
	public void openInputStream() {
		/* Stubbed */ 
	} // end method openInputStream
	
	
	/**
	 * 
	 */ 
	public void readObjects() {
		/* Stubbed */
	} // end method readObjects
	
	
	/**
	 * 
	 */ 
	public void closeInputStream() {
		/* Stubbed */ 
	} // end method closeInputStream
} // end class Calendar
