import java.util.ArrayList; 
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Creates instance of Event and stores them as local 
 * variables in main. Reads text from file, parses it, 
 * and creates Events based on it. Writes Events out 
 * to a text file. 
 *
 * @author	Madeleine kenny
 * @version	02/02/1017
 */ 
public class EventTextProcessor {
    public static void main(String[] args) {
		ArrayList<Event> events = new ArrayList<Event>(); // List of Events to process. 

		BufferedReader textInput = null;
		BufferedWriter textOutput = null;
		
		// Add two new events to the Calendar. 
        events.add(new Event( "Redmann's Party", "2014-03-03 22:00", "Redmann's House", "Come pass a good time" ));
        events.add(new Event( "CSCI 2120 Test", "2014-03-24 16:30", "MATH 226", "2nd Test" ));
		
		/* Create Events from info contained in a text file. */ 
		//Open a buffered character-based stream to read Events from. 
		  try{
        	textInput = new BufferedReader(new FileReader("eventData.txt"));
        }
        catch (IOException e) {
        	System.out.println("Error Opening Text File"); 
        }
		
		// Read Events from the text file and adds them to the ArrayList. 
		try{
			String nextName = textInput.readLine();
			while(nextName != null) {
				String nextDate = textInput.readLine();
				String nextLocation = textInput.readLine();
				String nextDescription = textInput.readLine();
				Event nextEvent = new Event(nextName, nextDate, nextLocation, nextDescription);
				events.add(nextEvent);
				textInput.readLine();
				nextName = textInput.readLine();
			} //end loop
		}
		catch(IOException e) {
			System.out.println("Error Reading from text file");
			System.exit(1); 
		}

		// Close the buffered character-based stream used to create Events.
		try{
			textInput.close();
		}
		catch(IOException e) {
			System.out.println("Error closing the file");
			System.exit(1);
		}
		
		/* Write the Events in the ArrayList out to a text file. */
		// Open a text file for writing.
		try{
			textOutput = new BufferedWriter(new FileWriter("myEvents.txt"));
		} 
		catch(IOException e) {
			System.out.println("Error opening the file to write to");
		}


		// Add the Events from the ArrayList to the text file. 
		try{
			for (Event event : events) {
				textOutput.write(event.toString() + "\n" + "\n");
			}
		}
		catch(IOException e) {
			System.out.println("Error writing to text file");
			System.exit(1);
		}
		
		// Close the text file. 
		try{
			textOutput.close();

		}
		catch(IOException e) {
			System.out.println("Error closing the file");
			System.exit(1);
		}
		
		// Print the Events. 
		for (Event event : events) {
			System.out.println(event.toString());
			System.out.println(); 
		}
    } // end method main
} // end class EventTextProcessor