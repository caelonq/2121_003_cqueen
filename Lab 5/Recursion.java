public class Recursion{
	

	public static int factorial(int n)
	{	

			return factorial(n, 1);
	}


	private static int factorial(int n, int product)
	{	

		if ( n >= 1)
			return factorial( n - 1, product * n);
		else
			return product;


	}


}