
import java.util.ArrayList;

public class SelectionSort
{
	public static void main(String[] args) {
		
		ArrayList<Integer> listj = new ArrayList<Integer>();
		int[] intsArray = {-1212, 13, 7, 7896, 10192, 4573625, -12, -1, 0, 1, 18};

		for (int i = 0; i < intsArray.length; i++) {
			listj.add(intsArray[i]); 
		} 

		sort(listj);

		for(Integer k: listj)
		{
			System.out.println(k);
		}

	}

	public static <T extends Comparable<T>> void sort(ArrayList<T> list)
	{

		sort(list, 0);

	}


	public static <T extends Comparable<T>> void sort(ArrayList<T> list, int startIndex)
	{

		if(startIndex < list.size()-1)
		{	
			swap(list, startIndex, findMin(list, startIndex));
			sort(list, 1 + startIndex);
		}

	}

	public static <T extends Comparable<T>> int findMin(ArrayList<T> list, int startIndex)
	{
		for(int i = startIndex; i <list.size(); ++i)
		{
			if(list.get(i).compareTo(list.get(startIndex)) < 0)
				startIndex = i;
		}
			   return startIndex;

	}


	private static <T extends Comparable<T>> void swap(ArrayList<T> list, int firstIndex, int secondIndex)
	{
		T temp = list.get(firstIndex);              
		list.set(firstIndex, list.get(secondIndex));
		list.set(secondIndex, temp);
											
	}
}