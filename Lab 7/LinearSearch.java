import java.util.ArrayList;


public class LinearSearch
{

	public static <T extends Comparable<T>> int linearSearch(ArrayList<T> list, T value)
	{
		for(T values: list)
		{
			if( value.compareTo(values) == 0)
				return list.indexOf(values);
		}
			return -1;
		

	}

}